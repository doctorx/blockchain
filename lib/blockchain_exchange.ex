defmodule Blockchain_Exchange do

    def parse(response) do
        Poison.decode(response.body)
    end

    def list_currencies() do
        "https://blockchain.info/ticker"
    end

    def exchange(value, :rub) do
        "https://blockchain.info/tobtc?currency=RUB&value=#{value}"
    end

    def exchange(value, _) do
         "https://blockchain.info/tobtc?currency=USD&value=#{value}"
    end

end